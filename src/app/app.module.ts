import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BiosBuscadorComponent } from './bios-buscador/bios-buscador.component';
import { BiosTramitesComponent } from './bios-tramites/bios-tramites.component';
import { BiosOpinionComponent } from './bios-opinion/bios-opinion.component';
import { BiosInformateComponent } from './bios-informate/bios-informate.component';
import { BiosOtrosTemasComponent } from './bios-otros-temas/bios-otros-temas.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BiosBuscadorComponent,
    BiosTramitesComponent,
    BiosOpinionComponent,
    BiosInformateComponent,
    BiosOtrosTemasComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
