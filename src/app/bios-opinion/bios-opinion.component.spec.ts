import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosOpinionComponent } from './bios-opinion.component';

describe('BiosOpinionComponent', () => {
  let component: BiosOpinionComponent;
  let fixture: ComponentFixture<BiosOpinionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosOpinionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosOpinionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
