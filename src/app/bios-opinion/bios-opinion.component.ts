import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-bios-opinion',
  templateUrl: './bios-opinion.component.html',
  styleUrls: ['./bios-opinion.component.css']
})
export class BiosOpinionComponent implements OnInit {
  url = '../../assets/json/opiniones.json';
  opiniones;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(this.url).subscribe((data) => this.displayData(data));
  }

  displayData(data)
  {
    this.opiniones = data;
  }

}
