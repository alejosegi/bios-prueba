import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosBuscadorComponent } from './bios-buscador.component';

describe('BiosBuscadorComponent', () => {
  let component: BiosBuscadorComponent;
  let fixture: ComponentFixture<BiosBuscadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosBuscadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosBuscadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
