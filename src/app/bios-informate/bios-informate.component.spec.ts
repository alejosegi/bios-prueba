import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosInformateComponent } from './bios-informate.component';

describe('BiosInformateComponent', () => {
  let component: BiosInformateComponent;
  let fixture: ComponentFixture<BiosInformateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosInformateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosInformateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
