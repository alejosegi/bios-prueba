import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosTramitesComponent } from './bios-tramites.component';

describe('BiosTramitesComponent', () => {
  let component: BiosTramitesComponent;
  let fixture: ComponentFixture<BiosTramitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosTramitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
