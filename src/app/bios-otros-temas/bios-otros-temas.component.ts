import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-bios-otros-temas',
  templateUrl: './bios-otros-temas.component.html',
  styleUrls: ['./bios-otros-temas.component.css']
})
export class BiosOtrosTemasComponent implements OnInit {
  url = '../../assets/json/otros-temas.json';
  informaciones;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(this.url).subscribe((data) => this.displayData(data));
  }

  displayData(data)
  {
    this.informaciones = data;
  }

}
