import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosOtrosTemasComponent } from './bios-otros-temas.component';

describe('BiosOtrosTemasComponent', () => {
  let component: BiosOtrosTemasComponent;
  let fixture: ComponentFixture<BiosOtrosTemasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosOtrosTemasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosOtrosTemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
